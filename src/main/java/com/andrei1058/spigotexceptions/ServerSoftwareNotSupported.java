package com.andrei1058.spigotexceptions;

public class ServerSoftwareNotSupported extends Throwable {

    public ServerSoftwareNotSupported(String message){
        super(message);
    }
}
