package com.andrei1058.spigotexceptions;

public class ServerVersionNotSupported extends Throwable {

    public ServerVersionNotSupported(String message){
        super(message);
    }
}
